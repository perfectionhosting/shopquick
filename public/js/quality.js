/**
 * Created by Administrator on 1/14/2017.
 */
$(document).ready(function () {
    var counter = 0;


    $("#ShopTable").jsGrid({
        height: "auto",
        width: "100%",

        filtering: false,
        inserting: true,
        editing: true,
        sorting: false,
        paging: true,
        autoload: true,
        pageLoading: true,

        pageSize: 15,
        pageButtonCount: 5,

        deleteConfirm: "Do you really want to delete your item?",

        controller: {
            loadData: function(filter) {
                var d = $.Deferred();
                $.ajax("/api/load_data", {
                data: {
                    filter: filter,
                    _token: window.Laravel.csrfToken
                },
                method: "GET"
                }).done(function (data) {
                    d.resolve(data);
                    update_price();
                });

                return d.promise();
            },
            insertItem: function(item) {
                var d = $.Deferred();
                $.ajax("/api/add_item", {
                    data: {
                        item: item,
                        _token: window.Laravel.csrfToken
                    },
                    method: "POST"
                }).done(function (data) {
                    d.resolve(data);
                    update_price();
                    window.setTimeout("reinstate()", 1000);
                });

                return d.promise();
            },
            updateItem: function(item) {
                var d = $.Deferred();
                $.ajax("/api/update_item", {
                    data: {
                        itemUPC: item.UPC,
                        amount: item.Quantity,
                        _token: window.Laravel.csrfToken
                    },
                    method: "POST"
                }).done(function (data) {
                    d.resolve(data);
                    update_price();
                });

                return d.promise();
            },
            deleteItem: function(item) {
                var d = $.Deferred();
                $.ajax("/api/del_item", {
                    data: {
                        itemUPC: item.UPC,
                        _token: window.Laravel.csrfToken
                    },
                    method: "POST"
                }).done(function (data) {
                    d.resolve(data);
                    update_price();
                });

                return d.promise();
            }
        },

        fields: [
            { name: "UPC", width: 60, editing: false},
            { name: "ItemName", type: "text", width: 150, editing: false },
            { name: "Amount", width: 60, editing: false },
            { name: "Quantity", type: "number", width: 40 },
            { type: "control" }
        ]
    });

    $("input.jsgrid-insert-mode-button").on('click', function () {
        $("tr.jsgrid-insert-row > td.jsgrid-cell > input:text")
        // don't navigate away from the field on tab when selecting an item
        .autocomplete({
            source: function( request, response ) {
                $.getJSON( "/api/search_item", {
                    term: request.term
                }, response );
            }
        });
    });

});
function update_price() {
    console.log("nice");
    $.ajax('/api/get_pricing', {
        data: {
            _token: window.Laravel.csrfToken
        }
    }).done(function (data) {
        var highCount = 0;
        var lowMoney = 99999999.99;
        var bestStore = "";
        $.each(data.totals, function (store, total) {
            if(data.totalAmount[store] > highCount) {
                highCount = data.totalAmount[store];
                lowMoney = total;
                bestStore = store;
            } else if(data.totalAmount[store] == highCount) {
                if(total < lowMoney) {
                    lowMoney = total;
                    bestStore = store;
                }
            }
        });
        $("#pricing").html("$" + lowMoney);
    });
}

function reinstate() {

        $("tr.jsgrid-insert-row > td.jsgrid-cell > input:text")
        // don't navigate away from the field on tab when selecting an item
            .autocomplete({
                source: function( request, response ) {
                    $.getJSON( "/api/search_item", {
                        term: request.term
                    }, response );
                }
            });
}