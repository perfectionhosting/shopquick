/**
 * Created by Administrator on 1/15/2017.
 */
var selected = "";
$(function() {
    $.ajax('/api/get_pricing', {
        data: {
            _token: window.Laravel.csrfToken
        },
        method: "GET"
    }).done(function (data) {
        var i = 0;
        $.each(data.totals, function (store, price) {
            var string = "<tr>";
            string += "<td>" + store + "</td>";
            if(data.missingItems[store] != undefined) {
                string += "<td><p>This store did not have the following:</p>";
                string += "<ul>";
                $.each(data.missingItems[store], function (index, missing) {
                    string += "<li>" + missing + "</li>";
                });
                string += "</ul>";
            } else {
                string += "<td>✓</td>";
            }
            string += "<td>" + price + "</td>";
            string += "<td><input type='radio' name='selected' value='" + store + "'></td>";
            $("#store_totals").append(string);
            i++;
        });
    });
    $("#continue").click(function () {
        window.location.href = "http://shopquick.store/finished/" + $("[name=selected]:checked").val();
    });
});

function select(i) {
    selected = i;
}
