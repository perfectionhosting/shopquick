<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/faq', function () {
   return view('faq');
});

Route::post('/api/add_item', 'ListController@add_item');
Route::get('/api/load_data', 'ListController@load_items');
Route::post('/api/update_item', 'ListController@change_qty');
Route::post('/api/del_item', 'ListController@del_item');
Route::get('/api/search_item', 'ListController@search_item');
Route::get('/api/get_pricing', 'FinalizeController@get_pricing');

Route::get('/shop', function () {
    return view('shop');
});

Route::get('/finished/{lmao}', function ($lmao) {
    $main = \App\ListObj::whereOwner(Auth::user()->email)->join('products', 'lists.upc', '=', 'products.upc')
        ->join('locations', 'lists.upc', '=', 'locations.upc')
        ->select('lists.*', 'products.name', 'products.brand', 'locations.aisle', 'locations.shelf')
        ->where('locations.store', $lmao)->orderBy('aisle', 'asc')->get();
    return view('finished', ['items'=>$main]);
});