@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="container-home">
                        <h1>Welcome to QuickShop!</h1>
                        <p>The one stop app to help you shop more efficiently and effectively in your life.</p>
                        <img src="https://media.giphy.com/media/fAhOtxIzrTxyE/giphy.gif" style="padding-bottom: 15px">
                        <img src="http://www.totusapp.com/img/comingsoon.png" style="padding-bottom: 15px">

                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <div class="container-footer">
            <div class="text-muted-footer">All Rights Reserved ShopQuick 2017® | Connect with us! <a href="https://www.facebook.com"><img src="https://cdn1.iconfinder.com/data/icons/logotypes/32/square-facebook-24.png"></a> <a href="https://www.Twitter.com"><img src="https://cdn1.iconfinder.com/data/icons/logotypes/32/square-twitter-24.png"></a> <a href="https://www.Instagram.com"><img src="https://cdn1.iconfinder.com/data/icons/logotypes/32/instagram-24.png"></a> <a href="https://www.plus.google.com"><img src="https://cdn1.iconfinder.com/data/icons/logotypes/32/square-google-plus-24.png"></a> </div>
        </div>
    </footer>
@endsection
