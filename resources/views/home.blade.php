@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div id="ShopTable"></div>
            </div>
            <div class="col-md-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Total Price:</div>
                    <div class="panel-body">
                        <p id="pricing">Loading...</p>
                        <a href="{{url('/shop')}}" class="btn btn-primary">Go Shop!</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <footer class="footer">
        <div class="container-footer">
            <div class="text-muted-footer">All Rights Reserved ShopQuick 2017® | Connect with us! <a href="https://www.facebook.com"><img src="https://cdn1.iconfinder.com/data/icons/logotypes/32/square-facebook-24.png"></a> <a href="https://www.Twitter.com"><img src="https://cdn1.iconfinder.com/data/icons/logotypes/32/square-twitter-24.png"></a> <a href="https://www.Instagram.com"><img src="https://cdn1.iconfinder.com/data/icons/logotypes/32/instagram-24.png"></a> <a href="https://www.plus.google.com"><img src="https://cdn1.iconfinder.com/data/icons/logotypes/32/square-google-plus-24.png"></a> </div>
        </div>
    </footer>
    <script>

    </script>
@endsection
