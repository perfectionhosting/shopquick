@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <table class="table table-striped table-bordered" id="ShopTable2">
                    <thead>
                    <tr>
                        <th>Store:</th>
                        <th>Availability:</th>
                        <th>Total Price:</th>
                        <th>Choose:</th>
                    </tr>
                    </thead>
                    <tbody id="store_totals">

                    </tbody>

                </table>
                <tr>
                    <td colspan="5" style="text-align: left;">
                        <input type="button" class="btn btn-lg btn-primary btn-block" id="continue" value="Continue" />
                    </td>
                </tr>
            </div>
        </div>
    </div>
    <footer class="footer">
        <div class="container-footer">
            <div class="text-muted-footer">All Rights Reserved ShopQuick 2017® | Connect with us! <a href="https://www.facebook.com"><img src="https://cdn1.iconfinder.com/data/icons/logotypes/32/square-facebook-24.png"></a> <a href="https://www.Twitter.com"><img src="https://cdn1.iconfinder.com/data/icons/logotypes/32/square-twitter-24.png"></a> <a href="https://www.Instagram.com"><img src="https://cdn1.iconfinder.com/data/icons/logotypes/32/instagram-24.png"></a> <a href="https://www.plus.google.com"><img src="https://cdn1.iconfinder.com/data/icons/logotypes/32/square-google-plus-24.png"></a> </div>
        </div>
    </footer>
    <script></script>
@endsection

@section("extraJS")
    <script src="/js/quality-two.js"></script>
@endsection
