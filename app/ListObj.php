<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListObj extends Model
{
    //
    protected $table = "lists";

    protected $guarded = [];
}
