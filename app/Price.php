<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Price
 *
 * @property string $upc
 * @property float $price
 * @property string $storename
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Price whereUpc($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Price wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Price whereStorename($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Price whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Price whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Price extends Model
{
    //
}
