<?php

namespace App\Http\Controllers;

use App\ListObj;
use App\Product;
use App\Products;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ListController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function view_list() {
        return view('list');
    }

    public function load_items(Request $request) {
        if($request->ajax()) {
            $index = $request->get("filter.pageIndex", 0) - 1;
            $size = $request->get("filter.pageSize", 15);
            /** @var ListObj $items */
            $items = ListObj::whereOwner(Auth::user()->email)
                ->join('products', 'lists.upc', '=', 'products.upc')->skip($index * $size)->limit($size)
                ->select('lists.*', 'products.name', 'products.amount')->get();
            $array = [];
            $array['itemsCount'] = ListObj::whereOwner(Auth::user()->email)->count();
            foreach($items as $item) {
                $array['data'][] = ['UPC'=>$item->upc, "ItemName"=>$item->name, 'Amount'=>$item->amount,'Quantity'=>$item->count];
            }
            if(!array_has($array, 'data')) {
                $array['data'] = [];
            }
            return response()->json($array);
        } else {
            throw new NotFoundHttpException("AJAX Request needed.");
        }
    }

    public function add_item(Request $request) {
        if($request->ajax()) {
            $data = $request->all();
            $validator = Validator::make($data, [
                'item.ItemName' => "required|min:8|max:12|exists:products,upc",
                'item.Quantity'  => "required|min:1|max:99"
            ]);
            if($validator->fails()) {
                return response()->json(array('success' => 'false', 'errors' => $validator->errors()));
            } else {
                /** @var ListObj $user_list */
                $user_list = ListObj::firstOrNew(array('owner' => Auth::user()->email, 'upc' => $request->input('item.ItemName')));
                $user_list->count += $request->input('item.Quantity');
                if($user_list->count > 99) {
                    $user_list->count = 99;
                }
                $user_list->save();
                return response()->json(['UPC'=>$user_list->upc, "ItemName"=>$user_list->name, 'Amount'=>'Garbage', 'Quantity'=>$user_list->count]);
            }
        } else {
            throw new NotFoundHttpException("AJAX Request needed.");
        }
    }

    public function del_item(Request $request)
    {
        if ($request->ajax()) {
            $data = $request->all();
            $validator = Validator::make($data, [
                'itemUPC' => "required|min:8|max:12|exists:lists,upc"
            ]);
            if ($validator->fails()) {
                return response()->json(array('success' => 'false', 'errors' => $validator->errors()));
            } else {
                /** @var ListObj $item */
                $item = ListObj::whereOwner(Auth::user()->email)->where('upc', $request->input('itemUPC'))->first();
                $item->delete();
                return response()->json(['success' => 'true']);
            }
        } else {
            throw new NotFoundHttpException("AJAX Request needed.");

        }
    }

    public function change_qty(Request $request) {
        if ($request->ajax()) {
            $data = $request->all();
            $validator = Validator::make($data, [
                'itemUPC' => "required|min:8|max:12|exists:lists,upc",
                'amount' => "required|min:1|max:99"
            ]);
            if ($validator->fails()) {
                return response()->json(array('success' => 'false', 'errors' => $validator->errors()));
            } else {
                /** @var ListObj $item */
                $item = ListObj::whereOwner(Auth::user()->email)->where('lists.upc', $request->input('itemUPC'))
                    ->join('products', 'lists.upc', '=', 'products.upc')
                    ->select('lists.*', 'products.name', 'products.amount')->first();
                $item->count = $request->input('amount');
                $item->save();
                return response()->json(['UPC'=>$item->upc, 'ItemName'=>$item->name, 'Amount'=>$item->amount, 'Quantity'=>$item->count]);
            }
        } else {
            throw new NotFoundHttpException("AJAX Request needed.");

        }
    }

    public function search_item(Request $request) {
        if ($request->ajax()) {
            $query = $request->input('term');
            $products = Product::where('name', 'LIKE', "%{$query}%")->limit(10)->get();
            $array = [];
            foreach($products as $product) {
                $array[] = ['label'=>$product->brand . " " .  $product->name, 'value'=>$product->upc];
            }
            return response()->json($array);
        } else {
            throw new NotFoundHttpException("AJAX Request needed.");
        }
    }
}
