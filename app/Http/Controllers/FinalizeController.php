<?php

namespace App\Http\Controllers;

use App\ListObj;
use App\Price;
use Auth;
use Illuminate\Http\Request;

class FinalizeController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function get_pricing() {
        $list = ListObj::whereOwner(Auth::user()->email)->join('products', 'lists.upc', '=', 'products.upc')
            ->select('lists.*', 'products.name')->get();
        $item_price_relation = [];
        $storenames = [];
        $missingItems = [];
        $upc_name_relation = [];
        $totals = [];
        $totalAmount=[];
        foreach($list as $list_item) {
            $stores = Price::where('upc', $list_item->upc)->get();
            $upc_name_relation[$list_item->upc] = $list_item->name;
            foreach($stores as $store) {
                $item_price_relation[$list_item->upc][$store->storename] = $store->price * $list_item->count;
                if(!in_array($store->storename, $storenames)) {
                    $storenames[] = $store->storename;
                }
            }
        }
        foreach($item_price_relation as $upc=>$data) {
            foreach($storenames as $storename) {
                if(!key_exists($storename, $data)) {
                    $missingItems[$storename][] = $upc_name_relation[$upc];
                } else {
                    if(isset($totals[$storename])) {
                        $totals[$storename] += $item_price_relation[$upc][$storename];
                        $totalAmount[$storename] += 1;
                    } else {
                        $totals[$storename] = floatval($item_price_relation[$upc][$storename]);
                        $totalAmount[$storename] = 1;
                    }
                }
            }
        }
        return response()->json(['missingItems' => $missingItems, 'totals'=>$totals, 'totalAmount'=>$totalAmount]);
    }
}
