<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Products
 *
 * @property string $upc
 * @property string $name
 * @property string $brand
 * @property string $safename
 * @property string $amount
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Products whereUpc($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Products whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Products whereBrand($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Products whereSafename($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Products whereAmount($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Products whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Products whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Product extends Model
{
    //
}
