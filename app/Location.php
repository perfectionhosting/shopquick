<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Location
 *
 * @property string $upc
 * @property int $aisle
 * @property int $shelf
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Location whereUpc($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Location whereAisle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Location whereShelf($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Location whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Location whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Location extends Model
{
    //
}
